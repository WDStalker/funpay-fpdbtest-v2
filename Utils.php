<?php

namespace FpDbTest;

use Exception;

class Utils
{
    /**
     * Checking for array associativity.
     *
     * @param array $items
     * @return bool
     */
    public static function isAssocArray(array $items): bool
    {
        if ($items === []) {
            return false;
        }
        return array_keys($items) !== range(0, count($items) - 1);
    }
}
