<?php

namespace FpDbTest;

use Exception;
use mysqli;
	
class Database implements DatabaseInterface
{
    private $current_db;

    public function __construct($db)
    {
    	if ($db instanceof mysqli) {
    		$this->current_db = new db\MySQL();
    	}
    }

    /**
     * Query builder
     *
     * @param string $query The line to be processed
     * @param array $args
     * @return query
     */
    public function buildQuery(string $query, array $args = []): string
    {
        if(!count($args)) {
            return $query;
        }

        return $this->current_db->process($query, $args);
    }

    /**
     * Argument which blocks to delete
     *
     * @param 
     * @return string
     */
    public function skip()
    {
    	return 'block';
    }
}
