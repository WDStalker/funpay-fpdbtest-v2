<?php
namespace FpDbTest\db;

use FpDbTest\Utils;

class MySQL
{
    /**
     * @var const array
     * Rules for specifiers
     */
    private const array qualifiers = [
        '?a' =>
            [
                'preg' => '\?a',
                'type' => '',
                'quote' => "'",
            ],
        '?#' =>
            [
                'preg' => '\?#',
                'type' => 'string',
                'quote' => "`",
            ],
        '?d' =>
            [
                'preg' => '\?d',
                'type' => 'int',
                'quote' => "",
            ],
        '?f' =>
            [
                'preg' => '\?f',
                'type' => 'float',
                'quote' => "",
            ],
        '{}' =>
            [
                'preg' => '\{[^}]+\}',
                'type' => '',
                'quote' => "",
            ],
        '?' =>
            [
                'preg' => '\?',
                'type' => 'string',
                'quote' => "'",
            ],
        ];

    /**
     * @var const string
     * block definition stub
     */
    private const string stub = '##$##';
	
    private string $query = '';
    private array $args = [];

    /**
     * Starts the string processing process
     *
     * @param string $query The line to be processed
     * @param array $args
     * @return query
     */	
    public function process(string $query, array $args = []): string
    {
        $this->query = $query;
        $this->args = $args;

        $this->searchQualifiers();

        //We remove the stub for blocks everywhere
        $this->query = str_replace(self::stub, '', $this->query);

        return $this->query;
    }
	
    /**
     * Finds a specifier in a string. Recursive method
     *
     * @param 
     * @return query
     */
    private function searchQualifiers(): void
    {
        $stop = false;
        $search = [];

        foreach (self::qualifiers as $keyQualifier => $Qualifier) {

            preg_match(
                '~'. $Qualifier['preg'] .'~is',
                $this->query,
                $matches,
                PREG_OFFSET_CAPTURE
            );

            if (isset($matches[0]) && count($matches[0])) {
                foreach ($matches as $key => $mat) {
                    $search[$keyQualifier] = ['found' => $mat[0], 'offset' => $mat[1]];
                }
            }
        }

        array_multisort (
            array_column($search, 'offset'),
            SORT_ASC,
            array_column($search, 'found'),
            SORT_DESC,
            $search
        );
		
        reset($search);
        reset($this->args);
		
        if (key($search) == '{}') {

            if (is_array(current($this->args))) {
                throw new Exception('The query structure is broken');
            }

            if (strstr(current($search)['found'], current($this->args))) {
                $this->query = str_replace(current($search)['found'], '', $this->query);
                $stop = true;
            } else {
                //Installing a stub for the block
                $found = str_replace(['{', '}'], self::stub, current($search)['found']);
                $this->query = str_replace(current($search)['found'], $found, $this->query);
            }
        } else {
            if (isset(current($search)['offset'])) {
                $this->query = $this->queryPrepare(
                    $this->query,
                    current($this->args),
                    key($search),
                    current($search)['offset']
                );
            } else {
                throw new Exception('The query structure is broken');
            }
            unset($this->args[key($this->args)]);
        }

        if (count($this->args)) {
            if (!$stop) {
                $this->searchQualifiers();
            }
        }
    }

    /**
     * Preparing argument.
     *
     * @param string $query The string in which we change the specifier
     * @param array|string|null|bool $arg Argument to replace
     * @param string $qualifier specifier
     * @param int $offset The position where the specifier is located in the string
     * @return query
     */
    private function queryPrepare(
        string $query,
        array|string|null|bool $arg,
        string $qualifier,
        int $offset
    ): string
    {
        if (is_array($arg)) {
            if (!Utils::isAssocArray($arg)) {

                $str = [];

                foreach ($arg AS $val) {
                    $str[] = $this->typePrepare($val, '', self::qualifiers[$qualifier]['quote']);
                }

                return $this->queryReplace($query, implode(', ', $str), $qualifier, $offset);

            } else {

                $str = [];

                foreach ($arg as $key => $val) {
                    $str[] = "`" . $key . "` = " . $this->typePrepare($val, '', "'");
                }

                return $this->queryReplace($query, implode(', ', $str), $qualifier, $offset);
            }
        } else {
            return $this->queryReplace(
                $query,
                $this->typePrepare(
                    $arg,
                    self::qualifiers[$qualifier]['type'],
                    self::qualifiers[$qualifier]['quote']
                ),
                $qualifier,
                $offset
            );
        }
    }

    /**
     * Preparing type variable.
     *
     * @param string|null|bool|int|float $val
     * @param string $quote_string Quotes for string
     * @return variable
     */
    private function typePrepare(
        string|null|bool|int|float $val,
        string $type,
        string $quote_string
    ): string
    {
        try {
            if ($type == '') {
                $val = match(get_debug_type($val)) {
                    'string' => $quote_string . (string)$val . $quote_string,
                    'int', 'bool' => (int)$val,
                    'float' => (float)$val,
                    'null' => 'NULL',
                };

            } else {
                $val = match($type) {
                    'string' => $quote_string . (string)$val . $quote_string,
                    'int', 'bool' => (int)$val,
                    'float' => (float)$val,
                    'NULL' => 'NULL',
                };
            }
        } catch (\UnhandledMatchError $e) {
            print('Implicit variable type:' . $val);
        }
        return $val;
    }

    /**
     * Replacement in string.
     *
     * @param string $query The string in which we change the specifier
     * @param array|string|null|bool $arg Argument to replace
     * @param string $qualifier specifier
     * @param int $offset The position where the specifier is located in the string
     * @return query
     */
    private function queryReplace(
        string $query,
        string $arg,
        string $qualifier,
        int $offset
    ): string
    {
        $query = substr_replace($query, $arg, $offset, strlen($qualifier));

        if ($offset === false) {
            throw new Exception('Params not enough to build query');
        }

        return $query;
    }
}
